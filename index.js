const RESULTS = {
    BAD : 'bad',
    GOOD : 'good',
    PENDING: 'pending'
};


class ArrayOfAsyncFuncs {
    constructor() {
        const length = random(5, 10);
        const funcs = (new Array(length)).fill().map(createFunc);
        let doneCounter = 0;

        funcs.result = RESULTS.PENDING;

        function createFunc(_, index) {
            return function func() {
                return new Promise((resolve, reject) => {setTimeout(
                    () => {
                        const result = new Result(index, doneCounter++);
                        if (!result.isOk) funcs.result = RESULTS.BAD;
                        if (doneCounter === length && funcs.result === RESULTS.PENDING) {
                            funcs.result = RESULTS.GOOD;
                        } 
                        (random(0, 10) >= 3 ? resolve : reject)(result);
                    },
                    random(100, 1000)
                )});
            }
        }

        return funcs;
    }
}

class Result {
    constructor(shouldBe, is) {
        this.shouldBe = shouldBe;
        this.is = is;
        this.isOk = shouldBe === is;
    }

    toString() {
        return this.isOk ? RESULTS.GOOD : RESULTS.BAD;
    }
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min + 1;
}

module.exports = ArrayOfAsyncFuncs;
